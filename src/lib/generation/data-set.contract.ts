export default interface IDataSet {
    name: string;
    order: number;
    female: { [key: string]: Array<[string, number]> };
    male: { [key: string]: Array<[string, number]> };
}
