export default abstract class Learner {
    public static learn(chunks: string[], order: number = 1): { [start: string]: Array<[string, number]> } {
        const starts: { [start: string]: Array<[string, number]> } = {};

        for (const chunk of chunks) {
            if (chunk.length <= order) { continue; }

            for (let i = 0; i < chunk.length - order - 1; i++) {
                const precedenceA = chunk.substr(i, order);
                const valueA = chunk[i + order];
                const precedenceB = chunk.substr(i + 1, order);
                const valueB = chunk[i + order + 1];

                if (starts[precedenceA] === undefined) { starts[precedenceA] = [ ]; }
                if (starts[precedenceB] === undefined) { starts[precedenceB] = [ ]; }

                if (starts[precedenceA].find(n => n[0] === valueA) === undefined) { starts[precedenceA].push([valueA, 0]); }
                if (starts[precedenceB].find(n => n[0] === valueB) === undefined) { starts[precedenceB].push([valueB, 0]); }

                const nodeA = starts[precedenceA].find(n => n[0] === valueA);
                const nodeB = starts[precedenceB].find(n => n[0] === valueB);

                const yolo = ['foo', 1];

                nodeA![1]++;
            }
        }

        return starts;
    }
}
