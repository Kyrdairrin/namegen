import RandomUtils from '../utils/random-utils';
import IOUtils from '../utils/io-utils';

const maxLength = 10;

export default abstract class Generator {
    public static generate(knowledge: { [start: string]: Array<[string, number]> }, count: number = 10, order: number = 1): string[] {
        const result = [];

        for (let i = 0; i < count; i++) {
            const startKey = RandomUtils.selectRandomKey(knowledge);

            let nodes = knowledge[startKey];
            let name = startKey;
            let node: [string, number] | null;

            do {
                node = RandomUtils.selectRandomWeighted(nodes);
                name += node === null ? '' : node[0];
                nodes = knowledge[name.slice(-order)];
            } while (name.length <= maxLength && node !== null);

            result.push(name);
        }

        return result;
    }

    public static generateFromSet(setName: string, male: boolean = false, count: number = 10) {
        const set = IOUtils.loadDataSet(setName);
        return this.generate(male ? set.male : set.female, count, set.order);
    }
}
