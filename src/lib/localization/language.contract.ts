export default interface ILanguage {
    key: string;
    flag: string;
    name: string;
    english: string | null;
}
