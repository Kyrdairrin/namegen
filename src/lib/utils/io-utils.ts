import IDataSet from '../generation/data-set.contract';

export default abstract class IOUtils {
    public static loadDataSet(name: string): IDataSet {
        return require(`@/data/${name}.json`) as IDataSet;
    }
}
