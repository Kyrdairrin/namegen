export default abstract class RandomUtils {
    public static selectRandom<T>(array: T[]): T {
        return array[Math.floor(Math.random() * array.length)];
    }

    public static selectRandomWeighted(nodes: Array<[string, number]>): [string, number] | null {
        const totalWeight: number = nodes.map(t => t[1]).reduce((w1, w2) => w1 + w2);
        let n = Math.floor(Math.random() * totalWeight);

        for (const node of nodes) {
            if (n < node[1]) { return node; }
            n -= node[1];
        }

        return null;
    }

    public static selectRandomKey(obj: object): string {
        return Object.keys(obj)[Math.floor(Math.random() * Object.keys(obj).length)];
    }
}
