import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
// import { localize, localizeMany } from './lib/localization/localize';
import i18n from './i18n';

Vue.config.productionTip = false;

// Vue.mixin({
//   filters: {
//     localize,
//     localizeMany,
//   },
// });

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
