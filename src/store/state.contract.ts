export default interface IState {
    availableSets: string[];
    names: string[];
    setName: string;
    male: boolean;
    fullName: boolean;
    count: number;
}
