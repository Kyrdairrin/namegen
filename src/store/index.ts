import Vue from 'vue';
import Vuex from 'vuex';
import IState from './state.contract';
import { GENERATE, CHANGE_SET, CHANGE_GENDER, CHANGE_COUNT } from './action-types';
import { MUTATE_SET, MUTATE_NAMES, MUTATE_GENDER, MUTATE_COUNT } from './mutation-types';
import Generator from '@/lib/generation/generator';

Vue.use(Vuex);

const initialState: IState = {
  count: 20,
  fullName: false,
  male: true,
  names: [],
  setName: 'english',
  availableSets: [ 'english', 'french' ],
};

export default new Vuex.Store({
  state: initialState,
  mutations: {
    [MUTATE_SET](state: IState, setName: string) {
      state.setName = setName;
    },
    [MUTATE_NAMES](state: IState, names: string[]) {
      state.names = names;
    },
    [MUTATE_GENDER](state: IState) {
      state.male = !state.male;
    },
    [MUTATE_COUNT](state: IState, count: number) {
      state.count = count;
    },
  },
  actions: {
    [CHANGE_SET](context, setName) {
      context.commit(MUTATE_SET, setName);
    },
    [GENERATE]({ commit, state }) {
      const names = Generator.generateFromSet(state.setName, state.male, state.count);
      commit(MUTATE_NAMES, names);
    },
    [CHANGE_GENDER](context, gender) {
      if (context.state.male === (gender !== 'male')) {
        context.commit(MUTATE_GENDER);
      }
    },
    [CHANGE_COUNT]({ commit }, count) {
      commit(MUTATE_COUNT, count);
    },
  },
});
