import Learner from '@/lib/generation/learner';
import names from '../../../data/names.json';

describe('Learner', () => {
    describe('learn', () => {
        it('learns when given proper data', () => {
            const knowledgeF1 = Learner.learn(names.female);
            const knowledgeF2 = Learner.learn(names.female, 2);
            const knowledgeM1 = Learner.learn(names.male);
            const knowledgeM2 = Learner.learn(names.male, 2);

            expect(Object.keys(knowledgeF1).length).toBeGreaterThan(10);
            expect(Object.keys(knowledgeF2).length).toBeGreaterThan(10);
            expect(Object.keys(knowledgeM1).length).toBeGreaterThan(10);
            expect(Object.keys(knowledgeM2).length).toBeGreaterThan(10);
        });
    });
});
