import Learner from '@/lib/generation/learner';
import Generator from '@/lib/generation/generator';
import names from '../../../data/names.json';

describe('Generator', () => {
    describe('generate', () => {
        it('with proper knowledge', () => {
            const knowledgeF1 = Learner.learn(names.female);
            const knowledgeF2 = Learner.learn(names.female, 2);
            const knowledgeF3 = Learner.learn(names.female, 3);
            const knowledgeM1 = Learner.learn(names.male);
            const knowledgeM2 = Learner.learn(names.male, 2);
            const knowledgeM3 = Learner.learn(names.male, 3);

            const namesF1 = Generator.generate(knowledgeF1, 15, 1);
            const namesF2 = Generator.generate(knowledgeF2, 15, 2);
            const namesF3 = Generator.generate(knowledgeF3, 15, 3);
            const namesM1 = Generator.generate(knowledgeM1, 15, 1);
            const namesM2 = Generator.generate(knowledgeM2, 15, 2);
            const namesM3 = Generator.generate(knowledgeM3, 15, 3);

            expect(namesF1.length).toBe(15);
            expect(namesF2.length).toBe(15);
            expect(namesF3.length).toBe(15);
            expect(namesM1.length).toBe(15);
            expect(namesM2.length).toBe(15);
            expect(namesM3.length).toBe(15);
        });
    });
});
