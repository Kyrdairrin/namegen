# NameGen

<img src="./src/assets/machine.png" height="100px" />

A VueJS application to generate names.

## Project setup

```console
yarn install
```

### Compiles and hot-reloads for development

```console
yarn serve
```

### Compiles and minifies for production

```console
yarn build
```

### Run your tests

```console
yarn test
```

### Lints and fixes files

```console
yarn lint
```

### Run your end-to-end tests

```console
yarn test:e2e
```

### Run your unit tests

```console
yarn test:unit
```

### Customize configuration

See [Vue CLI Configuration Reference](https://cli.vuejs.org/config/).

## License

![AGPLv3][agpl]

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

[agpl]: src/assets/agplv3-medium.png

## Credits

Project icon made by [Nikita Golubev](https://www.flaticon.com/authors/nikita-golubev "Nikita Golubev") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon") is licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0").

Flag icons made by [Freepik](http://www.freepik.com/ "Freepik") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon") are licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0").