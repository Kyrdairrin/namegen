nuget restore
Import-Module ./packages/Supernova.Scripts.Licensing.0.3.0/Licensing.psm1

Write-Output "***** Ready *****"

function ulh {
    Update-LicenseHeaders agpl 2018 "Kyrdairrin (Nicolas Maurice)" NameGen ./packages/Supernova.Scripts.Licensing.0.3.0/Templates
}
