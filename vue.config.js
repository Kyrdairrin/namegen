module.exports = {
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/namegen/'
    : '/',

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: './lib/localization/languages',
      enableInSFC: true
    }
  }
}
